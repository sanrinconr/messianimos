package messito;

public abstract class EstadoAnimo{
public final static int ALEGRE=3;
public final static int TRISTE=2;
public final static int INDIFERENTE=1;
public abstract EstadoAnimo cambiarEstado(int mensaje);
public abstract void imprimirAnimo();
}